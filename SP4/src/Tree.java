import java.util.Queue;
import java.util.LinkedList;

public class Tree {

    TreeNode root;
    static final long p = 999959;

    Tree() {
        root = new TreeNode(0);
    }

    /**
     * binary tree node
     */
    public class TreeNode {

        long data, height, depth;  // height and depth stores the height an depth of that node 
        TreeNode left, right, parent;

        TreeNode(long x) {
            data = x;
            left = null;
            right = null;
            parent = null;
            //For the root node. The height and depth initialised as 0.
            depth = 0;
            height = 0;
        }

        /**
         * create a new node that is attached to par node as left child if
         * goLeft is true; otherwise, the new node is attached to par as right
         * child
         */
        TreeNode(long x, TreeNode par, boolean goLeft) {
            data = x;
            left = null;
            right = null;
            parent = par;
            //Depth of a new node is depth of it parent + 1
            this.depth = par.depth + 1;
            //Height of newly added leaf is zero.
            this.height = 0;

            if (goLeft) {
                par.left = this;
            } else {
                par.right = this;
            }

            /*
             Adjust the height of the nodes from the newly added node to all the way up to the root 
             */
            TreeNode temp = this;
            while (temp.parent != null && 1 + temp.height > temp.parent.height) {
                temp.parent.height = 1 + temp.height;
                temp = temp.parent;
            }
        }
    } // end of TreeNode class

    // If there is a command line argument, it is used as the depth of the tree generated
    public static void main(String[] args) {
        long depth = 100000;
        if (args.length > 0) {
            depth = Long.parseLong(args[0]);
        }
        Tree x = new Tree();

        // Create a tree composed of 2 long paths 
        TreeNode last = x.root;
        for (long i = 1; i <= depth; i++) {
            last = x.new TreeNode(i, last, true);
        }

        last = x.root;
        for (long i = 1; i <= depth; i++) {
            last = x.new TreeNode(depth + i, last, false);
        }

       
        long s = System.currentTimeMillis();
        // The tree is visited in level order, using a Queue.  Depth and height of each node is computed recursively
        long minSum = Long.MAX_VALUE;
        Queue<TreeNode> Q = new LinkedList<>();
        Q.add(x.root);
        while (!Q.isEmpty()) {
            TreeNode cur = Q.remove();
            if (cur != null) {
                minSum = Math.min(minSum, _someFunction(cur));
                Q.add(cur.left);
                Q.add(cur.right);
            }
        }
        long e = System.currentTimeMillis();
        System.out.println("Answer: " + minSum + " " + (e - s) + "ms");
    }

    static long someFunction(TreeNode cur) {
        return rotater(rotater(depth(cur) * depth(cur)) % p + rotater(height(cur) * height(cur)) % p);
    }

    /*
     Function that compute the value directly from the instance variables of 
     height and depth rathe calculating at every 
     */
    static long _someFunction(TreeNode cur) {
        return rotater(rotater(cur.depth * cur.depth) % p + rotater(cur.height * cur.height) % p);
    }

    static long rotater(long h) {
        h ^= (h >>> 20) ^ (h >>> 12);
        h = (h ^ (h >>> 7) ^ (h >>> 4));
        return h;
    }

    // find the depth of a node 
    static long depth(TreeNode t) {
        return t.parent == null ? 0 : 1 + depth(t.parent);
    }

    // height of a node
    static long height(TreeNode t) {
        return t == null ? -1 : 1 + Math.max(height(t.left), height(t.right));
    }

    
}
