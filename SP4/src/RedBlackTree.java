public class RedBlackTree<T extends Comparable<? super T>> extends BST<T> {

	private static final boolean BLACK = false;
	private static final boolean RED = true;
	// Null Leaf Nodes
	private Entry<T> nullEntry;

	/**
	 * Constructor to set nullEntry and Root.
	 */
	RedBlackTree() {
		nullEntry = new Entry<>(null, nullEntry, nullEntry, null, BLACK);
		root = new Entry<>(null, nullEntry, nullEntry, null);		
	}

	/*
	 * Overridden to check for nullEntry instead of null
	 */
	@Override
	Entry<T> find(Entry<T> t, T x) {
		Entry<T> pre = t;
		while (t != nullEntry) {
			pre = t;
			int cmp = x.compareTo(t.element);
			if (cmp == 0) {
				return t;
			} else if (cmp < 0) {
				t = t.left;
			} else {
				t = t.right;
			}
		}
		return pre;
	}

	/*
	 * Overriden to check for nullEntry instead of null
	 */
	@Override
	public boolean contains(T x) {
		Entry<T> node = find(root, x);
		return node == nullEntry ? false : x.equals(node.element);
	}

	// Add x to tree. If tree contains a node with same key, replace element by
	// x.
	// Returns true if x is a new element added to tree.
	@Override
	public boolean add(T x) {
		if (size == 0) {
			root = new Entry<>(x, nullEntry, nullEntry, null, BLACK);
		} else {
			Entry<T> node = find(root, x);
			int cmp = x.compareTo(node.element);
			if (cmp == 0) {
				node.element = x;
				return false;
			}
			Entry<T> newNode = new Entry<>(x, nullEntry, nullEntry, node, RED);
			if (cmp < 0) {
				node.left = newNode;
			} else {
				node.right = newNode;
			}
			// restructure the tree to satisfy red black tree properties
			repair(newNode);
		}
		size++;
		return true;
	}

	private void repair(Entry<T> newNode) {
		// checking if parent is red. Only then repair is needed
		if (getColor(newNode.parent) == RED) {
			Entry<T> uncleNode = getSibling(newNode.parent);
			Entry<T> grandParent = getGrandParent(newNode);
			boolean leftExtendNode = false;
			// case1: Uncle node is red. Recoloring is required.
			if (getColor(uncleNode) == RED) {
				grandParent.color = !grandParent.color;
				uncleNode.color = !uncleNode.color;
				newNode.parent.color = !newNode.parent.color;
				repair(grandParent);
			} // case 2: v is aligned with grandparent;checking for left-left
				// alignment or right-right alignment
			else if ((leftExtendNode = (grandParent.left == newNode.parent && newNode.parent.left == newNode) == true)
					|| (grandParent.right == newNode.parent && newNode.parent.right == newNode)) {
				
				Entry<T> grandGrandParent = grandParent.parent;
				if (grandGrandParent == null)// when grandparent is root
					rotate(grandParent, null, leftExtendNode, false);
				else {
					rotate(grandParent, grandGrandParent, leftExtendNode, grandGrandParent.left == grandParent);
					newNode.parent.parent = grandGrandParent;
				}
				// flip colors: Parent as black and grandParent as red
				newNode.parent.color = BLACK;
				grandParent.color = RED;
			} // case 3: v is not aligned with grandparent
			else {
				Entry<T> parent = newNode.parent;
				rotate(parent,grandParent,newNode == newNode.parent.left,grandParent.left == parent);				
				newNode.parent = grandParent;
				repair(parent);
			}
			root.color = BLACK;
		}

	}

	/**
	 * @param node - Node that gets rotated
	 * @param parent - parent of the node after rotation
	 * @param right - indicates whether to rotate right or left
	 * @param isLeftChild - indicates, after rotation, node becomes left child or right child of parent
	 */
	private void rotate(Entry<T> node, Entry<T> parent, boolean right, boolean isLeftChild) {
		if (right)
			rotateRight(node, parent, isLeftChild);
		else
			rotateLeft(node, parent, isLeftChild);		
	}

	/**
	 * @param node - node that gets rotated right
	 * @param parent - parent of the node after rotation
	 * @param isLeftChild - indicates, after rotation, node becomes left child or right child of parent
	 */
	private void rotateRight(Entry<T> node, Entry<T> parent, boolean isLeftChild) {
		Entry<T> left = node.left;
		node.left = left.right;
		left.right = node;
		// rotate right will make left node as parent of given node
		node.parent = left;
		setParent(left,parent,isLeftChild);
	}

	/**
	 * @param node - node that gets rotated left
	 * @param parent - parent of the node after rotation
	 * @param isLeftChild - indicates, after rotation, node becomes left child or right child of parent
	 */
	private void rotateLeft(Entry<T> node, Entry<T> parent, boolean isLeftChild) {
		Entry<T> right = node.right;
		node.right = right.left;
		right.left = node;
		// rotate left will make right node as parent of given node
		node.parent = right;
		setParent(right,parent,isLeftChild);
		
	}
	
	/**
	 * This method connects the new child after rotation to parent
	 * @param child - Child node
	 * @param parent - parent node
	 * @param isLeftChild - - indicates whether child is left or right of parent
	 */
	private void setParent(Entry<T> child,Entry<T> parent,boolean isLeftChild)
	{
		if(parent == null)
			root = child;
		else if(isLeftChild)
			parent.left = child;
		else
			parent.right = child;
	}
	

	/**
	 * @param node - node whose sibling will be returned
	 * @return - sibling node
	 */
	private Entry<T> getSibling(Entry<T> node) {
		return (node == null || node.parent == null) ? null
				: node.parent.left == node ? node.parent.right : node.parent.left;
	}

	/**
	 * @param node - node whose grandparent will be returned
	 * @return - grandparent
	 */
	private Entry<T> getGrandParent(Entry<T> node) {
		return node == null || node.parent == null ? null : node.parent.parent;
	}

	/**
	 * @param node - node whose color will be returned
	 * @return - black or red flag
	 */
	private boolean getColor(Entry<T> node) {
		return node == null ? BLACK : node.color;
	}

	// Inorder traversal of tree
	@Override
	void printTree(Entry<T> node) {
		if (node != nullEntry) {
			printTree(node.left);
			System.out.print(" " + node.element + (node.color == RED ? "R" : "B"));
			printTree(node.right);
		}
	}

	public static void main(String[] args) {
		RedBlackTree<Integer> testTree = new RedBlackTree<>();
		testTree.add(50);
		testTree.add(25);
		testTree.add(75);
		testTree.add(12);
		testTree.add(20);
		testTree.add(23);
		testTree.add(24);
		testTree.printTree();
		System.out.println(testTree.verifyAVLTree(testTree.root, Integer.MIN_VALUE, Integer.MAX_VALUE));
	}

}
