import java.util.*;
import java.util.logging.Level;

public class BST<T extends Comparable<? super T>> {
	class Entry<T> {
		T element;
		boolean color;
		Entry<T> left, right, parent;
		long height;

		Entry(T x, Entry<T> l, Entry<T> r, Entry<T> p) {
			element = x;
			left = l;
			right = r;
			parent = p;
			height = 1;
		}

		Entry(T x, Entry<T> l, Entry<T> r, Entry<T> p, boolean c) {
			this(x, l, r, p);
			color = c;
		}

		Entry(T x) {
			element = x;
		}
	}

	Entry<T> root;
	int size;

	BST() {
		root = null;
		size = 0;
	}

	/**
	 * @param arr
	 *            - sorted array based on which BST is built
	 */
	BST(T[] arr) {
		this();
		if (arr != null && arr.length > 0) {
			size = arr.length;
			root = buildTree(arr, 0, arr.length - 1, null);
		}
	}

	/**
	 * @param arr
	 *            - input array based on which tree is built
	 * @param start
	 *            - start index of the array to be considered
	 * @param end
	 *            - end index of the array to be considered
	 * @param parent
	 *            - parent of the node to be created
	 * @return returns root of the tree
	 */
	Entry<T> buildTree(T[] arr, int start, int end, Entry<T> parent) {
		Entry<T> node = null;
		if (start <= end) {
			int middle = (start + end) / 2;
			node = new Entry<T>(arr[middle]);
			node.parent = parent;
			node.left = buildTree(arr, start, middle - 1, node);
			node.right = buildTree(arr, middle + 1, end, node);
		}
		return node;
	}

	// Find x in subtree rooted at node t. Returns node where search ends.
	Entry<T> find(Entry<T> t, T x) {
		Entry<T> pre = t;
		while (t != null) {
			pre = t;
			int cmp = x.compareTo(t.element);
			if (cmp == 0) {
				return t;
			} else if (cmp < 0) {
				t = t.left;
			} else {
				t = t.right;
			}
		}
		return pre;
	}

	// Is x contained in tree?
	public boolean contains(T x) {
		Entry<T> node = find(root, x);
		return node == null ? false : x.equals(node.element);
	}

	// Add x to tree. If tree contains a node with same key, replace element by
	// x.
	// Returns true if x is a new element added to tree.
	public boolean add(T x) {
		if (size == 0) {
			root = new Entry<>(x, null, null, null);
		} else {
			Entry<T> node = find(root, x);
			int cmp = x.compareTo(node.element);
			if (cmp == 0) {
				node.element = x;
				return false;
			}
			Entry<T> newNode = new Entry<>(x, null, null, node);
			if (cmp < 0) {
				node.left = newNode;
			} else {
				node.right = newNode;
			}
		}
		size++;
		return true;
	}

	// Remove x from tree. Return x if found, otherwise return null
	public T remove(T x) {
		T rv = null;
		if (size > 0) {
			Entry<T> node = find(root, x);
			if (x.equals(node.element)) {
				rv = node.element;
				remove(node);
				size--;
			}
		}
		return rv;
	}

	// Called when node has at most one child. Returns that child.
	Entry<T> oneChild(Entry<T> node) {
		return node.left == null ? node.right : node.left;
	}

	// Remove a node from tree
	void remove(Entry<T> node) {
		if (node.left != null && node.right != null) {
			removeTwo(node);
		} else {
			removeOne(node);
		}
	}

	// remove node that has at most one child
	void removeOne(Entry<T> node) {
		if (node == root) {
			root = oneChild(root);
		} else {
			Entry<T> p = node.parent;
			if (p.left == node) {
				p.left = oneChild(node);
			} else {
				p.right = oneChild(node);
			}
		}
	}

	// remove node that has two children
	void removeTwo(Entry<T> node) {
		Entry<T> minRight = node.right;
		while (minRight.left != null) {
			minRight = minRight.left;
		}
		node.element = minRight.element;
		removeOne(minRight);
	}

	public static void main(String[] args) {

		BST<Integer> t = new BST<>();
		Scanner in = new Scanner(System.in);
		while (in.hasNext()) {
			int x = in.nextInt();
			if (x > 0) {
				System.out.print("Add " + x + " : ");
				t.add(x);
				t.printTree();
			} else if (x < 0) {
				System.out.print("Remove " + x + " : ");
				t.remove(-x);
				t.printTree();
			} else {
				Comparable[] arr = t.toArray();
				System.out.print("Final: ");
				for (int i = 0; i < t.size; i++) {
					System.out.print(arr[i] + " ");
				}
				System.out.println();
				return;
			}
		}
		// Testing level order traversal
		System.out.println("Level Order traversal");
		Comparable[] test1 = t.levelOrderArray();
		for (int i = 0; i < test1.length; i++)
			System.out.print(" " + test1[i]);
		System.out.println();
		System.out.println("Array Input Result");
		Integer[] arrayInput = new Integer[] { 1, 2, 5, 7, 8, 10, 12, 15, 17 };
		BST<Integer> testTree = new BST<>(arrayInput);
		testTree.printTree();
	}

	// Create an array with the elements using in-order traversal of tree
	public Comparable[] toArray() {
		Comparable[] arr = new Comparable[size];
		inOrder(root, arr, 0);
		return arr;
	}

	// Recursive in-order traversal of tree rooted at "node".
	// "index" is next element of array to be written.
	// Returns index of next entry of arr to be written.
	int inOrder(Entry<T> node, Comparable[] arr, int index) {
		if (node != null) {
			index = inOrder(node.left, arr, index);
			arr[index++] = node.element;
			index = inOrder(node.right, arr, index);
		}
		return index;
	}

	/**
	 * @return - returns array of elements in level order traversal
	 */
	public Comparable[] levelOrderArray() {
		Comparable[] arr = new Comparable[size];
		levelOrder(root, arr);
		return arr;
	}

	private void levelOrder(Entry<T> node, Comparable[] arr) {
		if (node != null) {
			int index = 0;
			Queue<Entry<T>> queue = new LinkedList<>();
			queue.add(node);
			// loop invariant: queue has elements which get added in level order
			// each element in queue is removed and set to array transforming
			// into level order traversal array
			while (!queue.isEmpty()) {
				Entry<T> tempNode = queue.remove();
				arr[index++] = tempNode.element;
				if (tempNode.left != null)
					queue.add(tempNode.left);
				if (tempNode.right != null)
					queue.add(tempNode.right);
			}
		}
	}

	public void printTree() {
		System.out.print("[" + size + "]");
		printTree(root);
		System.out.println();
	}

	// Inorder traversal of tree
	void printTree(Entry<T> node) {
		if (node != null) {
			printTree(node.left);
			System.out.print(" " + node.element);
			printTree(node.right);
		}
	}

	/*
	 * Method: veriyBSTProperty - verifies the BST property and also check that
	 * the element in a node is not null i/p: tree - root node of a tree
	 * leftLimit - this is the lower limit of value that the element can have
	 * rightLimit - this is the higher limit of value that the element can have
	 * 
	 * o/p: returns true if the tree satisfies the BST property and no element
	 * is null else return false
	 */
	boolean verifyBSTProperty(Entry<T> tree, T leftLimit, T rightLimit) {
		if (tree != null) {
			if ((tree.element != null) && (tree.element.compareTo(leftLimit) == 1)
					&& (tree.element.compareTo(rightLimit) <= 0)) {
				/*
				 * Left subtree's values should range between the root node's
				 * value and leftlimit
				 */
				boolean leftResult = verifyBSTProperty(tree.left, leftLimit, tree.element);
				/*
				 * Right subtree's values should range between the root node's
				 * value and teh right limit
				 */
				boolean rightResult = verifyBSTProperty(tree.right, tree.element, rightLimit);
				return leftResult && rightResult;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	/*
	 * Method: checkBalancedCondition - checks the |height of right subtree -
	 * height of left subtree| <= 1 i/p: root of the tree o/p: true if the tree
	 * is balanced false otherwise
	 */
	boolean checkBalanceCondition(Entry<T> root) {
		if (root == null) {
			return true;
		} else {
			long leftSubTreeHeight;
			if (root.left == null) {
				leftSubTreeHeight = 0; // height = 0.if node is null
			} else {
				leftSubTreeHeight = root.left.height;
			}
			long rightSubTreeHeight;
			if (root.right == null) {
				rightSubTreeHeight = 0; // height = 0.if node is null
			} else {
				rightSubTreeHeight = root.right.height;
			}
			long balanceCondition = Math.abs(rightSubTreeHeight - leftSubTreeHeight);
			if (balanceCondition > 1) {
				return false;
			} else {
				return checkBalanceCondition(root.left) && checkBalanceCondition(root.right);
			}
		}
	}

	/*
	 * Method: verifyAVLTree, verifies the correctness of AVL tree formed i/p:
	 * node - root node of a tree leftLimit - attribute for verifyBSTProperty
	 * rightLimit - attribute for verifyBSTProperty
	 */
	boolean verifyAVLTree(Entry<T> node, T leftLimit, T rightLimit) {
		if (verifyBSTProperty(node, leftLimit, rightLimit)) {
			if (checkBalanceCondition(node)) {
				return true;
			}
		}
		return false;
	}
}