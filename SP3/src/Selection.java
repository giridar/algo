import java.util.PriorityQueue;
import java.util.Random;

/**
 * @author G94 
 * This class provides methods to find kth largest element in a
 * given array using quickSelect algorithm and minHeap algorithm.
 */
public class Selection {
	private static int phase = 0;
	private static long startTime, endTime, elapsedTime;

	/**
	 * @param arr
	 *            - Given input array
	 * @param k
	 *            - the largest element to find in given array
	 * @return - returns kth largest element
	 *
	 */
	public static <T extends Comparable<? super T>> T quickSelect(T[] arr, int k) {
		int kthIndex = select(arr, 0, arr.length - 1, k);
		return kthIndex != -1 ? arr[kthIndex] : null;
	}

	/**
	 * @param arr
	 *            - Given input array
	 * @param p
	 *            - starting index of array to select
	 * @param r
	 *            - ending index of array to select
	 * @param k
	 *            - the largest element to find in given array
	 * @return returns the index of kth largest element
	 */
	public static <T extends Comparable<? super T>> int select(T[] arr, int p, int r, int k) {
		if (arr.length == 0 || k <= 0 || k > r - p + 1)
			return -1;
		// returns the index of randomized pivot after partition
		int q = partition(arr, p, r);

		if (r - q >= k)
			return select(arr, q + 1, r, k);
		else if (r - q == k - 1)
			return q;
		else
			return select(arr, p, q - 1, k - r + q - 1);
	}

	/**
	 * @param arr
	 *            - Given input array
	 * @param p
	 *            - starting index of array to select
	 * @param r
	 *            - ending index of array to select
	 * @return - the index of randomized pivot
	 */
	private static <T extends Comparable<? super T>> int partition(T[] arr, int p, int r) {
		Random randomGenerator = new Random();
		exchange(arr, p + randomGenerator.nextInt(r - p + 1), r);
		T pivotValue = arr[r];
		int j = p;
		// loop invariant: i iterates over from given left index to right index
		// if array element at i is less than or equal to pivot value, then
		// position is exchanged
		for (int i = p; i < r; i++) {
			if (arr[i].compareTo(pivotValue) <= 0) {
				exchange(arr, i, j);
				j++;
			}
		}
		exchange(arr, j, r);
		return j;
	}

	/**
	 * @param arr
	 *            - Given input array
	 * @param a
	 *            - index whose position needs to be swapped
	 * @param b
	 *            - index whose position needs to be swapped
	 */
	private static <T extends Comparable<? super T>> void exchange(T[] arr, int a, int b) {
		T temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}

	/**
	 * @param arr
	 *            - Given input array
	 * @param k
	 *            - the largest element to find in given array
	 * @return - returns the largest element in given array
	 */
	public static <T extends Comparable<? super T>> T selectPQ(T[] arr, int k) {
		PriorityQueue<T> pq = new PriorityQueue<>();
		for (int i = 0; i < k; i++)
			pq.add(arr[i]);

		for (int i = k; i < arr.length; i++) {
			if (arr[i].compareTo(pq.peek()) > 0) {
				pq.remove();
				pq.add(arr[i]);
			}
		}

		return pq.peek();
	}

	/**
	 * @param args
	 *            Driver method to test
	 */
	public static void main(String[] args) {
		// input element count
		int n = 1000000;
		// value of k
		int kthElement = 910000;
		Integer[] quickSelect = new Integer[n];
		Integer[] pqSelect = new Integer[n];
		for (int i = 0; i < n; i++)
			quickSelect[i] = i;

		shuffleArray(quickSelect);
		for (int i = 0; i < n; i++)
			pqSelect[i] = quickSelect[i];
		System.out.println(
				"Analysis of Time Taken to find " + kthElement + "th element in array of " + n + " elements");
		System.out.println("QuickSelect Analysis");
		System.out.println("--------------------");
		timer();
		System.out.println("QuickSelect " + quickSelect(quickSelect, kthElement));
		timer();
		System.out.println("HeapSelect Analysis");
		System.out.println("--------------------");
		timer();
		System.out.println("HeapSelect " + selectPQ(pqSelect, kthElement));
		timer();
	}

	/**
	 * Timer to calculate the running time
	 */
	public static void timer() {
		if (phase == 0) {
			startTime = System.currentTimeMillis();
			phase = 1;
		} else {
			endTime = System.currentTimeMillis();
			elapsedTime = endTime - startTime;
			System.out.println("Time: " + elapsedTime + " msec.");
			memory();
			phase = 0;
		}
	}

	/**
	 * This method determines the memory usage
	 */
	public static void memory() {
		long memAvailable = Runtime.getRuntime().totalMemory();
		long memUsed = memAvailable - Runtime.getRuntime().freeMemory();
		System.out.println("Memory: " + memUsed / 1000000 + " MB / " + memAvailable / 1000000 + " MB.");
	}

	private static <T extends Comparable<? super T>> void shuffleArray(T[] ar) {
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			exchange(ar, index, i);
		}
	}

}
