import java.util.*;

/**
 * Class to represent a graph
 */
class Graph<V extends Vertex, E extends Edge> implements Iterable<V> {
    private V[] verts; // array of vertices
    public int numNodes; // number of verices in the graph
    private Factory<V> vertexFactory;
    private Factory<E> edgeFactory;

    /**
     * Constructor for Graph
     * 
     * @param size
     *            : int - number of vertices
     * @param sample
     *            : V - sample object to clone from
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    @SuppressWarnings("unchecked")
    Graph(int size, Factory<V> vertexFactory, Factory<E> edgeFactory) {
	numNodes = size;
	verts = (V[]) new Vertex[size + 1];
	this.vertexFactory = vertexFactory;
	this.edgeFactory = edgeFactory;
	verts[0] = null;
	// create an array of Vertex objects
	for (int i = 1; i <= size; i++) {
	    verts[i] = (V) this.vertexFactory.newInstance();
	    verts[i].name = i;
	}
    }

    /**
     * Method to add an edge to the graph
     * 
     * @param a
     *            : int - one end of edge
     * @param b
     *            : int - other end of edge
     * @param weight
     *            : int - the weight of the edge
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
	    void addEdge(int a, int b, int weight) {
	Vertex u = verts[a];
	Vertex v = verts[b];
	Edge e = edgeFactory.newInstance();
	e.From = u;
	e.To = v;
	e.Weight = weight;
	u.Adj.add(e);
	v.Adj.add(e);
    }

    /**
     * Method to add an arc (directed edge) to the graph
     * 
     * @param a
     *            : int - the head of the arc
     * @param b
     *            : int - the tail of the arc
     * @param weight
     *            : int - the weight of the arc
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
	    void addDirectedEdge(int a, int b, int weight) {
	Vertex head = verts[a];
	Vertex tail = verts[b];
	Edge e = edgeFactory.newInstance();
	e.From = head;
	e.To = tail;
	e.Weight = weight;
	head.Adj.add(e);
	tail.inDegree++; // in-degree is added to the vertex to which the
			 // directed edge is pointing
	tail.revAdj.add(e);
    }

    /**
     * @return - the diameter of the graph. if the graph has cycle or if it is
     *         not connected, then it returns -1
     */
    public int getDiameter() {
	int diameter = 0;
	// if there is only one node, then diameter is zero
	if (this.numNodes > 1) {
	    // finding the longest vertex from vertex 1
	    Vertex longestVertex = BFSTraversal(verts[1]);

	    if (longestVertex == null)
		return -1;
	    // reset values
	    resetVertices();
	    // sets the diameter as the maximum distance from longest vertex
	    diameter = BFSTraversal(longestVertex).distance;
	}

	// reset values
	resetVertices();

	return diameter;
    }

    /**
     * This method resets seen flag, parent and distance of each vertices in
     * graph
     */
    private void resetVertices() {
	for (Vertex v : verts) {
	    if (v != null) {
		v.seen = false;
		v.parent = null;
		v.distance = 0;
	    }
	}
    }

    /**
     * @param source
     *            - Vertex from which BFS is performed
     * @return - Returns the vertex which is maximum distance from source
     */
    private Vertex BFSTraversal(Vertex source) {
	Queue<Vertex> queue = new LinkedList<>();
	source.seen = true;
	source.distance = 0;
	queue.add(source);
	Vertex longestVertex = source;
	int numberOfVertex = 1;
	// queue is polled until it is empty. BFS traversal is done. Distance is
	// updated as Parent's vertex distance plus one
	// longestVertex keeps track as vertex with maximum distance from source
	while (!queue.isEmpty()) {
	    Vertex u = queue.poll();
	    for (Edge e : u.Adj) {
		Vertex v = e.otherEnd(u);
		// if the edge is not seen already but the vertex is seen, then
		// there is a cycle, return null
		if (!e.seen && v.seen)
		    return null;
		e.seen = true;
		if (!v.seen) {
		    v.parent = u;
		    v.distance = u.distance + 1;
		    if (longestVertex.distance < v.distance)
			longestVertex = v;
		    v.seen = true;
		    queue.add(v);
		    numberOfVertex++;
		}
	    }
	}

	// Check if graph is disconnected
	if (numberOfVertex != numNodes)
	    return null;

	return longestVertex;
    }

    /**
     * Method to create an instance of VertexIterator
     */
    public Iterator<V> iterator() {
	return new VertexIterator();
    }

    /**
     * A Custom Iterator Class for iterating through the vertices in a graph
     * 
     *
     * @param <Vertex>
     */
    private class VertexIterator implements Iterator<V> {
	int index;

	/**
	 * Constructor for VertexIterator
	 * 
	 * @param v
	 *            : Array of vertices
	 * @param n
	 *            : int - Size of the graph
	 */
	private VertexIterator() {
	    index = 1; // Index 0 is not used. Skip it.
	}

	/**
	 * Method to check if there is any vertex left in the iteration
	 * Overrides the default hasNext() method of Iterator Class
	 */
	public boolean hasNext() {
	    return index < verts.length;
	}

	/**
	 * Method to return the next Vertex object in the iteration Overrides
	 * the default next() method of Iterator Class
	 */
	public V next() {
	    return verts[index++];
	}

	/**
	 * Throws an error if a vertex is attempted to be removed
	 */
	public void remove() {
	    throw new UnsupportedOperationException();
	}
    }

    /* Interface that has to be implemented for Vertex & Edge classes */
    public static interface Factory<T> {
	public T newInstance();
    }

    public static <V extends Vertex, E extends Edge> Graph<V, E> readGraph(Scanner in, boolean directed,
	    Factory<V> vertexFactory, Factory<E> edgeFactory) {
	// read the graph related parameters
	int n = in.nextInt(); // number of vertices in the graph
	int m = in.nextInt(); // number of edges in the graph

	// create a graph instance
	Graph<V, E> g = new Graph<V, E>(n, vertexFactory, edgeFactory);
	for (int i = 0; i < m; i++) {
	    int u = in.nextInt();
	    int v = in.nextInt();
	    int w = in.nextInt();
	    if (directed) {
		g.addDirectedEdge(u, v, w);
	    } else {
		g.addEdge(u, v, w);
	    }
	}
	in.close();
	return g;
    }
}