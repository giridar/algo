import java.util.Random;

/**
 *
 * @author G94
 * @param <T>
 */
class PartitionResult {

	int p1, p2;

	public PartitionResult(int p1, int p2) {
		this.p1 = p1;
		this.p2 = p2;
	}

}

public class Sort {

	private static int phase = 0;
	private static long startTime, endTime, elapsedTime;
	private static Random rand = new Random();
	private static final int THRESHHOLD = 13;

	/**
	 * @param inputArray
	 *            - Given input array
	 * @param left
	 *            - left position
	 * @param right
	 *            - right position This method does the sorting using insertion
	 *            sort algorithm.
	 */
	public static <T extends Comparable<? super T>> void insertionSort(T[] inputArray, int left, int right) {

		int j;
		// loop invariant: for each element in index i from left + 1 to right
		// compare with elements from i -1 till left position and insert in
		// sorted order
		for (int i = left + 1; i <= right; i++) {
			T tmp = inputArray[i];
			for (j = i; j > left && tmp.compareTo(inputArray[j - 1]) < 0; j--)
				inputArray[j] = inputArray[j - 1];
			inputArray[j] = tmp;
		}

	}

	/**
	 * @param inputArray
	 *            - Given input array This method does the sorting using
	 *            insertion sort algorithm.
	 */
	public static <T extends Comparable<? super T>> void mergeSort(T[] inputArray) {
		T[] tempArray = (T[]) new Comparable[inputArray.length];
		mergeSort(inputArray, tempArray, 0, inputArray.length - 1);

	}

	/**
	 * @param inputArray
	 *            - Given input array
	 * @param tempArray
	 *            - temp array of merge sort algorithm
	 * @param left
	 *            - left position
	 * @param right
	 *            - right position
	 */
	private static <T extends Comparable<? super T>> void mergeSort(T[] inputArray, T[] tempArray, int left,
			int right) {
		if (left < right) {
			int n = right - left + 1;
			if (n <= THRESHHOLD) {
				insertionSort(inputArray, left, right);
			} else {
				int middle = (left + right) >> 1;
				mergeSort(inputArray, tempArray, left, middle);
				mergeSort(inputArray, tempArray, middle + 1, right);
				merge(inputArray, tempArray, left, middle, right);
			}
		}
	}

	/**
	 * @param inputArray
	 *            - Given input array
	 * @param tempArray
	 *            - temp array of merge sort algorithm
	 * @param leftStart
	 *            - left position starting index
	 * @param middle
	 *            - middle index
	 * @param rightEnd
	 *            - right most index
	 */
	private static <T extends Comparable<? super T>> void merge(T[] inputArray, T[] tempArray, int leftStart,
			int middle, int rightEnd) {
		int rightStart = middle + 1;
		int initialLeftPosition = leftStart;
		int totalElements = rightEnd - leftStart + 1;
		// loop invariant: iterating from left to right end using index i
		// if left is smaller than right or right side is empty, then left side
		// value is stored in temp array
		for (int i = leftStart; i <= rightEnd; i++) {
			if (rightStart > rightEnd || (initialLeftPosition <= middle
					&& inputArray[initialLeftPosition].compareTo(inputArray[rightStart]) <= 0)) {
				tempArray[i] = inputArray[initialLeftPosition];
				initialLeftPosition++;
			} else {
				tempArray[i] = inputArray[rightStart];
				rightStart++;
			}
		}
		// copy back the sorted elements in temp array to input array
		for (int i = 0; i < totalElements; i++) {
			inputArray[rightEnd] = tempArray[rightEnd];
			rightEnd--;
		}
	}

	/**
	 * @param input
	 *            - Given input array
	 * @param p
	 *            - left index
	 * @param r
	 *            - right index This method does the sorting using quick sort
	 *            algorithm.
	 */
	private static <T extends Comparable<? super T>> void traditionalQuicksort(T[] input, int p, int r) {
		if (p < r) {
			int n = r - p + 1;
			if (n <= THRESHHOLD) {
				insertionSort(input, p, r);
			} else {
				int q = traditionalPartition(input, p, r);
				traditionalQuicksort(input, p, q - 1);
				traditionalQuicksort(input, q + 1, r);
			}
		}
	}

	/**
	 * @param input
	 *            - Given input array
	 * @param p
	 *            - left index
	 * @param r
	 *            - right index
	 * @return - returns the index of pivot This method partitions given input
	 *         array based on randomized pivot.
	 */
	public static <T extends Comparable<? super T>> int traditionalPartition(T[] input, int p, int r) {
		int pivotIndex = rand.nextInt(r - p + 1) + p;
		T pivot = input[pivotIndex];
		swap(input, pivotIndex, r);
		int i = p;
		for (int j = p; j < r; j++) {
			if (input[j].compareTo(pivot) == -1) {
				swap(input, i, j);
				i++;
			}
		}
		swap(input, r, i);
		return i;
	}

	/*
	 * Method: TwoPivotQuickSort - Sorts the elements by partioning the array
	 * into three ranges using two pivots i/p: input: input to be sorted p:
	 * start position of the array to be sorted r: end position of the array to
	 * be sorted
	 */
	public static <T extends Comparable<? super T>> void twoPivotQuicksort(T input[], int p, int r) {
		if (r - p + 1 > THRESHHOLD) { // Call two pivot partition only if the
										// number of
			// elements is greater than 2
			PartitionResult q = twoPivotPartition(input, p, r);
			twoPivotQuicksort(input, p, q.p1);
			if (input[q.p1] != input[q.p2]) { // If the elements at p1 and p2
												// are the same then all the
												// elements in that range are
												// the same and hence no need of
												// partition
				twoPivotQuicksort(input, q.p1, q.p2);
			}
			twoPivotQuicksort(input, q.p2, r);
		} else {
			insertionSort(input, p, r);
		}

	}

	/*
	 * Method: twoPivotPartition, Partions the array into 3 regions using two
	 * pivots i/p: input: array of elements to be partioned p: start of the
	 * range of array to be partitioned e: end of the range of array to be
	 * partitioned
	 */
	private static <T extends Comparable<? super T>> PartitionResult twoPivotPartition(T[] input, int p, int r) {

		int p1Index = rand.nextInt(r - p + 1) + p;
		int p2Index = rand.nextInt(r - p + 1) + p;
		/*
		 * Loop ensures the same index is not chosen for both of the pivots
		 */
		while (p1Index == p2Index) {
			p2Index = rand.nextInt(r - p + 1) + p;
		}
		T pivot1;
		T pivot2;
		/*
		 * pivot1<pivot2 Exchange pivot1 and the start index, pivot2 and the end
		 * index
		 */
		if (input[p1Index].compareTo(input[p2Index]) == -1) {
			pivot1 = input[p1Index];
			pivot2 = input[p2Index];
			swap(input, p1Index, p);
			swap(input, p2Index, r);
		} else {
			/*
			 * pivot1>=pivot2 Exchange pivot2 and the start index, pivot1 and
			 * the end index
			 */
			pivot2 = input[p1Index];
			pivot1 = input[p2Index];
			swap(input, p1Index, r);
			swap(input, p2Index, p);
		}

		int i = p + 1, l = p + 1, j = r - 1;

		while (i <= j) {

			if (input[i].compareTo(pivot1) >= 0 && input[i].compareTo(pivot2) <= 0) {
				i++;
			} else if (input[i].compareTo(pivot1) == -1) {
				swap(input, i, l);
				l++;
				i++;
			} else if (input[j].compareTo(pivot2) == 1) {
				j--;
			} else if (input[i].compareTo(pivot2) == 1 && input[j].compareTo(pivot1) == -1) {
				if (l != i) { // If l and i are different make a circular swap
								// among i,j, and l
					T temp = input[l];
					input[l] = input[j];
					input[j] = input[i];
					input[i] = temp;
				} else { // If l and i are the same index, swap elemnts at i and
							// j
					swap(input, i, j);
				}
				i++;
				j--;
				l++;
			} else if (input[j].compareTo(pivot1) >= 0 && input[j].compareTo(pivot2) <= 0
					&& input[i].compareTo(pivot2) == 1) {
				swap(input, i, j);
				i++;
				j--;
			}
		}
		swap(input, p, l - 1);
		swap(input, r, j + 1);
		return new PartitionResult(l - 1, j + 1);

	}

	public static void main(String[] args) {
		// input array length
		int n = 1048576;
		//is duplicate needed in input array
		boolean duplicate = true;
		
		Integer[] mergeSortInput = new Integer[n];
		Integer[] traditionalquickSortInput = new Integer[n];
		Integer[] dualPivotquickSortInput = new Integer[n];
		if (!duplicate) {
			for (int i = 0; i < n; i++)
				mergeSortInput[i] = i;
			shuffleArray(mergeSortInput);
		} else {
			Random inputRand = new Random();
			for (int i = 0; i < n; i++)
				mergeSortInput[i] = inputRand.nextInt(1000);
		}
		for (int i = 0; i < n; i++) {
			traditionalquickSortInput[i] = mergeSortInput[i];
			dualPivotquickSortInput[i] = mergeSortInput[i];
		}

		System.out.println("Analysis of merge sort and single pivot quick sort");
		System.out.println("Merge sort Analysis");
		System.out.println("-------------------");
		timer();
		mergeSort(mergeSortInput);
		timer();
		System.out.println("Quick Sort Analysis");
		System.out.println("-------------------");
		timer();
		traditionalQuicksort(traditionalquickSortInput, 0, n - 1);
		timer();
		System.out.println("Dual Pivot Quick Sort Analysis");
		System.out.println("------------------------------");
		timer();
		twoPivotQuicksort(dualPivotquickSortInput, 0, n - 1);
		timer();
	}

	/**
	 * Timer to calculate the running time
	 */
	public static void timer() {
		if (phase == 0) {
			startTime = System.currentTimeMillis();
			phase = 1;
		} else {
			endTime = System.currentTimeMillis();
			elapsedTime = endTime - startTime;
			System.out.println("Time: " + elapsedTime + " msec.");
			memory();
			phase = 0;
		}
	}

	/**
	 * This method determines the memory usage
	 */
	public static void memory() {
		long memAvailable = Runtime.getRuntime().totalMemory();
		long memUsed = memAvailable - Runtime.getRuntime().freeMemory();
		System.out.println("Memory: " + memUsed / 1000000 + " MB / " + memAvailable / 1000000 + " MB.");
	}

	private static <T extends Comparable<? super T>> void swap(T[] input, int i, int j) {
		T temp = input[i];
		input[i] = input[j];
		input[j] = temp;
	}

	private static <T extends Comparable<? super T>> void shuffleArray(T[] ar) {
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);

			swap(ar, index, i);
		}
	}
}
