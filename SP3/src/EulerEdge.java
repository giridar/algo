public class EulerEdge extends Edge {
    public EulerEdge() {
	super(null, null, 0);
    }

    public EulerEdge(Vertex u, Vertex v, int w) {
	super(u, v, w);
    }
}