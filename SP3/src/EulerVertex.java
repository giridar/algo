public class EulerVertex extends Vertex {
    public int cno; // the component number of the vertex in the graph
    public int edgesProcessed; // Keeps track of number of edges processed

    public EulerVertex() {
	super(0);
    }

    public EulerVertex(int n) {
	super(n);
    }
}