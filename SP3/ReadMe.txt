Short Project 3

f. Generic Graph
----------------
Objects of Generic types cannot be created within a class with Type arguments. 
So the Graph class constructor needs an extra argument to create the objects of 
the generic type at runtime. There are 2 ways of doing this. 

1.) Using reflection to get the constructor and create a new instance. This 
needs lesser code footprint and no re-work whenever the Vertex & Edge classes 
are extended. But this can fail for multiple reasons like if the constructor 
signature doesn't match or the constructor is not public.

2.) Creating Factory objects for the subclass types(EulerVertex, EulerEdge) and
passing them as arguments to readGraph and then to Graph constructor. The caveat 
here is that, since EulerVertex & EulerEdge are subclasses of Vertex & Edge, 
Factory objects of Vertex & Edge classes will also be accepted by the constructor
of Graph<EulerVertex, EulerEdge> and this will cause an exception at runtime. So
instead of creating concrete Factory classes we use Anonymous classes to avoid
using Vertex & Edge Factory objects being used in the place of EulerVertex & 
EulerEdge Factory objects.
