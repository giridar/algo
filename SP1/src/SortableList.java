import java.util.Arrays;

/**
 * @author G94 This Class extends SinglyLinkedList class implementing merge sort
 *         on linked lists.
 * @param <T>
 *            T represents any object that implements Comparable interface
 */
public class SortableList<T extends Comparable<? super T>> extends SinglyLinkedList<T> {

	/**
	 * This method passes the starting node of the list to sort
	 */
	public void mergeSort() {
		header.next = mergeSort(header.next);
	}

	/**
	 * @param head
	 *            - head of the list which has to be sorted and merged
	 * @return - returns the head of the list which is sorted
	 */
	private Entry<T> mergeSort(Entry<T> head) {

		if (head == null || head.next == null)
			return head;
		// Split the list and set middle node
		Entry<T> secondHead = getMiddleEntry(head);

		head = mergeSort(head);

		secondHead = mergeSort(secondHead);
		// merge two split list
		head = merge(head, secondHead);

		return head;
	}

	/**
	 * @param head
	 *            - head of the list which has to be split
	 * @return - returns the head of the second half of the list
	 */
	private Entry<T> getMiddleEntry(Entry<T> head) {
		Entry<T> slowRunner = head;
		Entry<T> fastRunner = head.next;
		// Invariant: fastRunner moves twice on each iteration
		// SlowRunner moves once on each iteration
		while (fastRunner != null) {
			fastRunner = fastRunner.next;
			if (fastRunner != null) {
				slowRunner = slowRunner.next;
				fastRunner = fastRunner.next;
			}
		}

		Entry<T> outHeader = slowRunner.next;
		// This statement separates the list into two halves.
		slowRunner.next = null;

		return outHeader;
	}

	private Entry<T> merge(Entry<T> firstHead, Entry<T> secondHead) {
		// Invariant: header node is the chain of merged sorted elements from
		// two list
		Entry<T> headPointer = new Entry<>();
		Entry<T> header = headPointer;
		while (firstHead != null && secondHead != null) {
			int compareValue = firstHead.element.compareTo(secondHead.element);
			if (compareValue <= 0) {
				header.next = firstHead;
				firstHead = firstHead.next;
			} else if (compareValue > 0) {
				header.next = secondHead;
				secondHead = secondHead.next;
			}
			header = header.next;
		}

		header.next = (firstHead == null) ? secondHead : firstHead;

		return headPointer.next;
	}

	public static void main(String[] args) {
		SortableList<Integer> test = new SortableList<>();
		for (int a : Arrays.asList(5, 1, 12, 6, 12, 8, 4, 14, 11, 9)) {
			test.add(a);
		}
		test.mergeSort();
		test.printList();
	}
}