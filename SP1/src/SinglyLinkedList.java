import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Generic Implementation of a Single Linked List
 */
public class SinglyLinkedList<T> {
	public static class Entry<T> {
		T element;
		Entry<T> next;

		Entry() {
		}

		Entry(T x, Entry<T> nxt) {
			element = x;
			next = nxt;
		}
	}

	Entry<T> header, tail;
	int size;

	SinglyLinkedList() {
		header = new Entry<>(null, null);
		tail = null;
		size = 0;
	}

	void add(T x) {
		if (tail == null) {
			header.next = new Entry<>(x, header.next);
			tail = header.next;
		} else {
			tail.next = new Entry<>(x, null);
			tail = tail.next;
		}
		size++;
	}

	void printList() {
		Entry<T> x = header.next;
		while (x != null) {
			System.out.print(x.element + " ");
			x = x.next;
		}
		System.out.println();
	}

	void multiUnzip(int k) {
		if (k < 2 || size <= k) {
			/* Too few elements. No change. */
			return;
		}

		List<Entry<T>> heads = new ArrayList<>(k);
		List<Entry<T>> tails = new ArrayList<>(k);
		Entry<T> c = header.next;
		for (int i = 0; i < k; i++) {
			heads.add(c);
			tails.add(c);
			c = c.next;
		}
		int state = 0;

		/*
		 * Invariant: tails.get(i) is the tail of the chain of elements with
		 * 'i'th index (i=0..k-1). c is the current element to be processed.
		 * state indicates the state of the finite state machine. state = i
		 * indicates that the current element is added after tails.get(i)
		 * (i=0..k-1).
		 */
		while (c != null) {
			tails.get(state).next = c;
			tails.set(state, c);
			c = c.next;
			state = ++state % k;
		}
		for (int i = 1; i < k; i++) {
			tails.get(i - 1).next = heads.get(i);
		}
		tail = tails.get(k - 1);
		tail.next = null;
	}

	public void reverseRecursive() {
		/* Check if the list is not empty. */
		if (header.next != null) {
			tail = _reverse(header.next);
			tail.next = null;
		}
	}

	private Entry<T> _reverse(Entry<T> head) {
		if (head.next == null) {
			/* Make the old tail as the new head. */
			header.next = head;
			return head;
		}

		/*
		 * Invariant: newTail is the tail of the reversed sub-list starting at
		 * current instance of head.next. Add the current head after the new
		 * tail on the return sequence.
		 */
		Entry<T> newTail = _reverse(head.next);
		newTail.next = head;
		return head;
	}

	public void reverseNonRecursive() {
		/* Check if there are atleast 2 elements in the list. */
		if (header.next != null && header.next.next != null) {
			Entry<T> p = header.next;
			header.next = tail;
			tail = p;
			Entry<T> q = p.next;
			p.next = null;
			while (q != null) {
				/*
				 * Invariant: 'p' is the head of the reversed sub-list. 'q' is
				 * the head of the un-reversed sub-list. Add the head of the
				 * un-reversed sub-list before the head of the reversed
				 * sub-list.
				 */
				Entry<T> r = q.next;
				q.next = p;
				p = q;
				q = r;
			}
		}
	}

	public void printReverseRecursive() {
		/* Check if the list is not empty. */
		if (header.next != null) {
			_printReverse(header.next);
		}
		System.out.println();
	}

	private void _printReverse(Entry<T> head) {
		if (head == null) {
			return;
		}

		/*
		 * Print the current element after the call to the sub-list returns.
		 */
		_printReverse(head.next);
		System.out.print(head.element + " ");
		return;
	}

	public void printReverseNonRecursive() {
		/* Check if the list is not empty. */
		Entry<T> p = header.next;
		if (p != null) {
			/* Push the entries into a Stack. */
			Stack<Entry<T>> s = new Stack<>();
			while (p != null) {
				s.push(p);
				p = p.next;
			}

			/* Pop the entries from the Stack and print them. */
			while (!s.empty()) {
				System.out.print(s.pop().element + " ");
			}
		}
		System.out.println();
	}

	public static void main(String[] args) {
		int n = 10; // Default size of linked list
		int k = 3; // Default order number

		SinglyLinkedList<Integer> l = new SinglyLinkedList<>();
		for (int i = 1; i <= n; i++) {
			l.add(new Integer(i));
		}

		System.out.println("Original List:");
		l.printList();
		l.multiUnzip(k);
		System.out.println("Unzipped List:");
		l.printList();

		l.reverseRecursive();
		System.out.println("Reversed List(w Recursion):");
		l.printList();
		l.reverseNonRecursive();
		System.out.println("Reversed List(w/o Recursion):");
		l.printList();

		System.out.println("List printed in reverse order(w Recursion):");
		l.printReverseRecursive();
		System.out.println("List printed in reverse order(w/o Recursion):");
		l.printReverseNonRecursive();
	}
}
