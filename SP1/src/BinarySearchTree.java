import java.util.Arrays;
import java.util.Stack;

public class BinarySearchTree<AnyType extends Comparable<? super AnyType>> {

	// Basic node stored in unbalanced binary search trees
	private static class BinaryNode<AnyType> {

		BinaryNode(AnyType theElement, BinaryNode<AnyType> lt, BinaryNode<AnyType> rt) {
			element = theElement;
			left = lt;
			right = rt;
		}

		AnyType element; // The data in the node
		BinaryNode<AnyType> left; // Left child
		BinaryNode<AnyType> right; // Right child
	}

	/** The tree root. */
	private BinaryNode<AnyType> root;

	public static enum traversalType {
		inOrder, preOrder, postOrder, inOrderStack, preOrderStack
	}

	/**
	 * Construct the tree.
	 */
	public BinarySearchTree() {
		root = null;
	}

	/**
	 * Insert into the tree; duplicates are ignored.
	 * 
	 * @param x
	 *            the item to insert.
	 */
	public void insert(AnyType x) {
		root = insert(x, root);
	}

	/**
	 * Test if the tree is logically empty.
	 * 
	 * @return true if empty, false otherwise.
	 */
	public boolean isEmpty() {
		return root == null;
	}

	public void printTree(traversalType traversalType) {
		if (!isEmpty()) {
			switch (traversalType) {
			case inOrder:
				System.out.println("Printing in In-Order: ");
				printInorder(root);
				break;
			case preOrder:
				System.out.println("Printing in Pre-Order: ");
				printPreorder(root);
				break;
			case postOrder:
				System.out.println("Printing in Post-Order: ");
				printPostorder(root);
				break;
			case inOrderStack:
				System.out.println("Printing in In-Order using stack: ");
				printInOrderStack(root);
				break;
			case preOrderStack:
				System.out.println("Printing in Pre-Order using stack: ");
				printPreOrderStack(root);
				break;
			default:
				break;
			}
			System.out.println();
		} else
			System.out.println(" Tree is empty.");
	}

	/**
	 * Internal method to insert into a subtree.
	 * 
	 * @param x
	 *            the item to insert.
	 * @param t
	 *            the node that roots the subtree.
	 * @return the new root of the subtree.
	 */
	private BinaryNode<AnyType> insert(AnyType x, BinaryNode<AnyType> t) {
		if (t == null)
			return new BinaryNode<>(x, null, null);

		int compareResult = x.compareTo(t.element);

		if (compareResult < 0)
			t.left = insert(x, t.left);
		else if (compareResult > 0)
			t.right = insert(x, t.right);
		else
			; // Duplicate; do nothing
		return t;
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 */
	private void printInorder(BinaryNode<AnyType> t) {
		if (t != null) {
			printInorder(t.left);
			System.out.print(t.element + "  ");
			printInorder(t.right);
		}
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 */
	private void printPreorder(BinaryNode<AnyType> t) {
		if (t != null) {
			System.out.print(t.element + "  ");
			printPreorder(t.left);
			printPreorder(t.right);
		}
	}

	/**
	 * Internal method to print a subtree in sorted order.
	 * 
	 * @param t
	 *            the node that roots the subtree.
	 */
	private void printPostorder(BinaryNode<AnyType> t) {
		if (t != null) {
			printPostorder(t.left);
			printPostorder(t.right);
			System.out.print(t.element + "  ");
		}
	}

	/**
	 * @param t
	 *            - node whose value and its children are printed in In-Order
	 *            using stack
	 */
	private void printInOrderStack(BinaryNode<AnyType> t) {
		if (t != null) {
			// stack which maintains the node to be printed in the in order
			Stack<BinaryNode<AnyType>> stack = new Stack<>();
			stack.push(t);
			// Traverse to left most node and keep adding the nodes to stack as
			// we traverse
			while (t.left != null) {
				t = t.left;
				stack.push(t);
			}

			// Every time a node is popped out, it is printed and its right
			// node(if any) is added to stack along with its left node.
			while (!stack.empty()) {
				BinaryNode<AnyType> printNode = stack.pop();
				System.out.print(printNode.element + "  ");
				if (printNode.right != null) {
					BinaryNode<AnyType> rightNode = printNode.right;
					stack.push(rightNode);
					while (rightNode.left != null) {
						stack.push(rightNode.left);
						rightNode = rightNode.left;
					}
				}
			}
		}
	}

	/**
	 * @param t
	 *            - node whose value and its children are printed in Pre-Order
	 *            using stack
	 */
	private void printPreOrderStack(BinaryNode<AnyType> t) {
		if (t != null) {

			Stack<BinaryNode<AnyType>> stack = new Stack<>();
			stack.push(t);
			// Every time a node is popped out from stack, following steps are
			// done
			// 1. Node element is printed
			// 2. Right node of popped out node, if any, is added to the stack
			// 3. Left node of popped out node, if any, is added to the stack
			while (!stack.empty()) {
				BinaryNode<AnyType> printNode = stack.pop();
				System.out.print(printNode.element + "  ");
				if (printNode.right != null)
					stack.push(printNode.right);
				if (printNode.left != null)
					stack.push(printNode.left);
			}
		}
	}

	// Test program
	public static void main(String[] args) {
		BinarySearchTree<Integer> t = new BinarySearchTree<>();
		for (int a : Arrays.asList(57, 12, 20, 108, 64, 11, 96)) {
			t.insert(a);
		}
		t.printTree(traversalType.preOrder);
		t.printTree(traversalType.preOrderStack);
		t.printTree(traversalType.inOrder);
		t.printTree(traversalType.inOrderStack);
	}

}
