Short Project 1

c. SortableList
---------------
Performs MergeSort on the given list
Sample I/P: 5 1 12 6 12 8 4 14 11 9

Sample O/P: 1 4 5 6 8 9 11 12 12 14


d. BinarySearchTree
-------------------
Recursive and Non-recursive implementation of a BinarySearchTree traversals
Sample I/P: 57, 12, 20, 108, 64, 11, 96

Sample O/P: Printing in Pre-Order: 
			57  12  11  20  108  64  96  
			Printing in Pre-Order using stack: 
			57  12  11  20  108  64  96  
			Printing in In-Order: 
			11  12  20  57  64  96  108  
			Printing in In-Order using stack: 
			11  12  20  57  64  96  108  

			
e&f. SinglyLinkedList
---------------------
Re-arranges a list by the given order and reverses a linkedlist with & without recursion
Sample I/P: 10 3

Sample O/P:	Original List:
			1 2 3 4 5 6 7 8 9 10 
			Unzipped List:
			1 4 7 10 2 5 8 3 6 9 
			Reversed List(w Recursion):
			9 6 3 8 5 2 10 7 4 1 
			Reversed List(w/o Recursion):
			1 4 7 10 2 5 8 3 6 9 
			List printed in reverse order(w Recursion):
			9 6 3 8 5 2 10 7 4 1 
			List printed in reverse order(w/o Recursion):
			9 6 3 8 5 2 10 7 4 1 
 

