What it does
------------
The program generates 'n' number of Integers in reverse order and sorts them 
using the specified algorithm.

How to run
----------
java Main <opt> <n>

Input
-----
opt - which sorting algorithm to use (0-MergeSert, 1-PriorityQueueSort)
n   - number of elements to sort

Output
------
Run time of the algorithm in ms
