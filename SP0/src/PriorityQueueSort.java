import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Generic implementation of sorting using PriorityQueue
 */
public class PriorityQueueSort<T extends Comparable<? super T>> extends Sort<T> {

	@Override
	public void sort(T[] a) {
		PriorityQueue<T> q = new PriorityQueue<>(Arrays.asList(a));
		for (int i = 0; !q.isEmpty(); i++) {
			a[i] = q.remove();
		}
	}

}
