import java.io.IOException;

/**
 * Driver class that generates Integers upto 'n' in reverse order and then calls
 * the specific sorting algorithm.
 */
public class Main {
	/*
	 * Print the first ten elements in the array
	 */
	static <T> void firstTen(T[] a) {
		int n = Math.min(a.length, 10);
		for (int i = 0; i < n; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) throws IOException {
		int opt = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);
		Integer[] a = new Integer[n];
		Integer[] tmp = new Integer[n];
		for (int i = 0; i < n; i++) {
			a[i] = new Integer(n - i);
		}

		Sort<Integer> algo;
		switch (opt) {
		case 0:
			algo = new MergeSort<>(tmp);
			break;
		case 1:
			algo = new PriorityQueueSort<>();
			break;
		default:
			algo = new MergeSort<>(tmp);
			break;
		}

		firstTen(a);
		long start = System.currentTimeMillis();
		algo.sort(a);
		long end = System.currentTimeMillis();
		firstTen(a);
		System.out.println("Total run time: " + (end - start) + " ms");
	}
}
