/**
 * Generic implementation of Merge sort
 */
public class MergeSort<T extends Comparable<? super T>> extends Sort<T> {
	T[] tmp;

	public MergeSort(T[] tmp) {
		this.tmp = tmp;
	}

	@Override
	public void sort(T[] a) {
		_sort(a, 0, a.length - 1);
	}

	private void _sort(T[] a, int p, int r) {
		if (p < r) {
			int q = (p + r) / 2;
			_sort(a, p, q);
			_sort(a, q + 1, r);
			_merge(a, p, q + 1, r);
		}
	}

	private void _merge(T[] a, int p, int q, int r) {
		int p1 = p;
		int q1 = q - 1;
		int p2 = q;
		int q2 = r;
		for (int i = p; i <= r; i++) {
			tmp[i] = a[i];
		}

		while (p1 <= q1 && p2 <= q2) {
			if (tmp[p1].compareTo(tmp[p2]) <= 0) {
				a[p++] = tmp[p1++];
			} else {
				a[p++] = tmp[p2++];
			}
		}
		while (p1 <= q1) {
			a[p++] = tmp[p1++];
		}
		while (p2 <= q2) {
			a[p++] = tmp[p2++];
		}
	}
}
