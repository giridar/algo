/**
 * Abstract class of generic type T that implements Comparable directly or by
 * inheritance. All sort algorithms should extend this class.
 */
public abstract class Sort<T extends Comparable<? super T>> {

	public abstract void sort(T[] a);

}
