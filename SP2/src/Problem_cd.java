import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

/**
 * @author G94
 */
public class Problem_cd {

    //-------------- Question 3 -------------------------------------
    
    //This method is the helper for _DFS1 to construct the vertices stack
    static void DFSVisit1(Vertex u, Stack<Vertex> verticesStack) {
        u.seen = true;
        List<Edge> edges;
        edges = u.Adj; // While building stack all the out going edges are considered.
        
        for (Edge e : edges) {
            Vertex v = e.otherEnd(u); // For every edge in the adjacency list, perform DFS visit with the other end(vertex) of that edge
            if (!v.seen) {
                DFSVisit1(v, verticesStack);
            }
        }
        verticesStack.push(u); // Push the processed vertices into the stack
    }

    //This method is the helper for _DFS2 to assign the component number to the vertices
    static void DFSVisit2(Vertex u, int componentNumber) {
        u.seen = true;
        List<Edge> edges;
        edges = u.revAdj;  // In this step the DFS must be done with reversed edges and hence reverse adjacency list is used
        for (Edge e : edges) {
            Vertex v = e.otherEnd(u); // For every edge in the adjacency list, perform DFS visit with the other end(vertex) of that edge
            if (!v.seen) {
                DFSVisit2(v, componentNumber);
            }
        }
    }

   
    /*
     This method performs DFS traversal and builds the vertices stack in the reverse order 
     */
    static void _DFS1(Graph graph, Stack<Vertex> verticesStack) {
        // Vertices Stack- Stack that contains vertices
        for (Vertex u : graph.verts) {
            if (u != null) {
                u.seen = false;
                u.parent = null;
            }
        }
        for (Vertex u : graph.verts) {
            if (u != null) {
                if (!u.seen) {
                    DFSVisit1(u, verticesStack); 
                }
            }
        }
    }

    //This method assigns the component number to vertices
    static int _DFS2(Graph graph, Stack<Vertex> verticesStack) {
        // Vertices Stack- Stack that contains vertices
        for (Vertex u : graph.verts) {
            if (u != null) {
                u.seen = false;
                u.parent = null;
            }
        }

        int componentNumber = 0;
        //VerticesStack - serves as s source of outer loop
        while (!verticesStack.isEmpty()) {
            Vertex u = verticesStack.pop();
            if (!u.seen) {
                DFSVisit2(u, ++componentNumber); // Everytime  a new vertices poped up from the stack and if it is not already processed, a new component begins.
            }
        }
        return componentNumber;
    }

    
    /*
     This function is a wrapper for building the stack of vertices
     */
    static Stack<Vertex> buildStackByDFS(Graph graph) {
        Stack<Vertex> verticesStack = new Stack<>();
        _DFS1(graph, verticesStack);
        return verticesStack;
    }

    /*
     This function is a wrapper for calculating the number of connected components in the graph
     */
    static int getConnectedComponents(Graph graph, Stack<Vertex> verticesStack) {
        return (_DFS2(graph, verticesStack));
    }

    /*
     Function finds the number of Strongly connected components in a graph. This takes place in two phases
     1.Build the stack of vertices in the reverse order while performing DFS search
     2.Perform the DFS search again with stack of vertices as the outer loop. 
     Assign component number while doing theis DFS traversal. 
    
     */
    static int stronglyConnectedComponents(Graph graph) {
        Stack<Vertex> verticesStack = buildStackByDFS(graph);
        int numberOfComponents = getConnectedComponents(graph, verticesStack);
        return numberOfComponents;
    }

    //------------------ Question 4 ---------------------------
    /*
     This method finds the vertices of the cycle
     */
    static List<Vertex> findOddLengthCycleVertices(Vertex u, Vertex v) {
        LinkedList<Vertex> uvertices = new LinkedList<>();
        LinkedList<Vertex> vvertices = new LinkedList<>();
        uvertices.addLast(u);
        vvertices.addFirst(v);
        //Loop helps to find the LCA of two vertice u and v
        while (u.parent != null && v.parent != null) {
            if (u.parent != v.parent) {
                u = u.parent;
                v = v.parent;
                uvertices.addLast(u); // Add u vertices -vertices from u to LCA
                vvertices.addFirst(v); // Add v vertice - vertices from v to LCA
            } else {
                uvertices.add(u.parent); // finally add LCA
                break;
            }
        }
        uvertices.addAll(vvertices); //Add all v vertices to uvertices and return uvertices
        return uvertices;
    }

    // The method performs BFS traversal of graph
    static List<Vertex> BFS(Graph g) {
        for (Vertex v : g) {
            v.seen = false;
            v.parent = null;
        }

        for (Vertex source : g) {
            if (!source.seen) {
                source.distance = 0;
                source.seen = true;
                Queue<Vertex> q = new ArrayDeque<>(); // Queue is used to store vertices to implement BFS
                q.add(source);

                while (!q.isEmpty()) {
                    Vertex u = q.remove();
                    for (Edge e : u.Adj) {
                        Vertex v = e.otherEnd(u);
                        if (!v.seen) {
                            v.seen = true;
                            v.parent = u; // Once a particular vertex(v) is reached as beacause of processing the edge of the other(u), then u is the  v's parent
                            v.distance = u.distance + 1; // assign the distance of v as one more than u as v is processed after u
                            q.add(v); // Add new vertex(v) into the queue for BFS traversal
                        } else {
                            if (u.distance == v.distance) { // if distacne of u and v are same, then odd length cylce is there in the graph
                                return (findOddLengthCycleVertices(u, v));
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /*Function that finds the oddLength cycle
     Algorithm: 
     Run BFS on graph. Find two vertices of same distance from arbitrary source.
     Find the least common ancestor of two vertices.
     All the the vertices from u and v to their common ancestor forms the list of vertices that consitute an odd length cycle
     */
    static List<Vertex> findOddLengthCycle(Graph g) {
        return (BFS(g));
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner s = new Scanner(new File(args[0]));
        Graph g = Graph.readGraph(s, true);
        System.out.println("Number of strongly connected components: " + stronglyConnectedComponents(g));

        s = new Scanner(new File(args[1]));
        g = Graph.readGraph(s, false);
        List<Vertex> result = findOddLengthCycle(g);
        if (result != null) {
            System.out.println("The graph is not bipartite. The vertices of one of the odd length cycle is:");
            for (Vertex v : result) {
                System.out.print(v.name + " ");
            }
        } else {
            System.out.println("The graph is bipartite");
        }
    }
}
