import java.util.*;
import java.io.*;

/**
 * @author Srikanth - This class has functions to perform topological sort in
 *         two different ways
 */
public class TopologicalSort {

    /**
     * @param g
     *            - Input graph for which topological sort needs to be
     *            determined
     * @return - List of vertices in topological order. This method uses queue
     *         to implement the topological sort
     * 
     */
    public static List<Vertex> toplogicalOrder1(Graph g) {
	List<Vertex> result = new ArrayList<>();
	// Invariant: queue keeps track of vertices whose in degree is zero.
	if (g != null) {
	    Queue<Vertex> queue = new LinkedList<>();
	    int vertexCount = 0; // counter to track if there is any cycle
	    // Add all the vertices whose in-degree of given graph is zero.
	    for (Vertex v : g.verts) {
		if (v != null && v.inDegree == 0) {
		    queue.add(v);
		    vertexCount++;
		}
	    }

	    // Keep adding vertices to the queue whose in-degree is zero. When a
	    // vertex is removed from queue, the in degree of vertices
	    // connecting with removed vertex is reduced by one
	    while (!queue.isEmpty()) {
		Vertex u = queue.remove();
		result.add(u);

		for (Edge e : u.Adj) {
		    Vertex v = e.otherEnd(u);
		    v.inDegree--;
		    if (v.inDegree == 0) {
			queue.add(v);
			vertexCount++;
		    }
		}
	    }

	    // If there is a cycle in graph, the processed vertex will not match
	    // with total vertex count.
	    if (vertexCount != g.numNodes) {
		System.out.println("Given directed grapgh has cycle. Topological sort works only in DAG.");
		return Collections.<Vertex> emptyList();
	    }
	}

	return result;
    }

    /**
     * @param g
     *            - Input graph for which topological sort needs to be
     *            determined
     * @return - Returns a stack object which is in topological sort when popped
     *         out. This method uses Depth First Search traversal to determine
     *         topological sort
     */
    public static Stack<Vertex> toplogicalOrder2(Graph g) {

	Stack<Vertex> stack = new Stack<>();

	if (g != null) {
	    // Pass the stack to DFS method to add the vertices in stack
	    for (Vertex u : g) {
		if (u != null && !u.seen)
		    DFSVisit(u, stack);
	    }
	}

	// If there is a cycle in graph, the processed vertex will not match
	// with total vertex count.
	if (stack.size() != g.numNodes) {
	    System.out.println("Given directed graph has cycle. Topological sort works only in DAG.");
	    return new Stack<Vertex>();
	}

	return stack;
    }

    /**
     * @param u
     *            - Input vertex which is traversed in depth
     * @param stack
     *            - Stack which will contain vertices in topological order
     * 
     */
    private static void DFSVisit(Vertex u, Stack<Vertex> stack) {
	u.seen = true;
	// Invariant: for all the edges associated to u, the other vertex is
	// examined and DFS for it is called recursively
	for (Edge e : u.Adj) {
	    Vertex v = e.otherEnd(u);
	    if (!v.seen) {
		v.parent = u;
		DFSVisit(v, stack);
	    }
	}
	// Finally the vertex that has all edge vertex seen is added to stack
	stack.push(u);
    }

    public static void main(String[] args) throws FileNotFoundException {
	Scanner in;
	if (args.length > 0) {
	    File inputFile = new File(args[0]);
	    in = new Scanner(inputFile);
	} else {
	    in = new Scanner(System.in);
	}
	Graph testGraph = Graph.readGraph(in, false);

	System.out.println("Diameter of given tree");
	System.out.println(testGraph.getDiameter());

	System.out.println("Printing in topological sort 2");
	Stack<Vertex> testList = TopologicalSort.toplogicalOrder2(testGraph);
	while (!testList.isEmpty())
	    System.out.print(testList.pop() + " ");
	System.out.println();

	System.out.println("Printing in topological sort 1");
	List<Vertex> sortedList = TopologicalSort.toplogicalOrder1(testGraph);
	for (Vertex v : sortedList)
	    System.out.print(v + " ");
    }

}
