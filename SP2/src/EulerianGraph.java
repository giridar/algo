import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Class to check if a graph is Eulerian
 * 
 * @author G94
 */
public class EulerianGraph {
    /**
     * Method to test if a graph is Eulerian. A connected graph G is called
     * Eulerian if the degree of every vertex is an even number. A connected
     * graph that has exactly 2 vertices of odd degree has an Eulerian path.
     * 
     * @param g
     *            : The graph to be tested
     */
    public static void testEulerian(Graph g) {
	if (connectedComponents(g) > 1) {
	    // The graph is not connected
	    System.out.println("Graph is not connected.");
	    return;
	}

	int oddVerts = 0;
	Vertex[] v = new Vertex[2];
	for (Vertex u : g) {
	    if (u.Adj.size() % 2 != 0) {
		/*
		 * Using mod of odd vertices count to store any 2 vertices of
		 * odd degree instead of an if-else block
		 */
		v[oddVerts % 2] = u;
		oddVerts++;
	    }
	}
	switch (oddVerts) {
	case 0:
	    // All vertices are of even degree
	    System.out.println("Graph is Eulerian.");
	    break;

	case 2:
	    // Exactly 2 vertices are of odd degree
	    System.out.println("Graph has an Eulerian Path between vertices "
		    + v[0] + " and " + v[1] + ".");
	    break;

	default:
	    // More than 2 vertices are of odd degree
	    System.out.println("Graph is not Eulerian.  It has " + oddVerts
		    + " vertices of odd degree.");
	    break;
	}
    }

    /**
     * Method to count the number of connected components in a graph using DFS
     * 
     * @param g
     *            : The graph to count the connected components for
     */
    public static int connectedComponents(Graph g) {
	for (Vertex u : g) {
	    u.seen = false;
	    u.parent = null;
	}
	int cno = 0;
	for (Vertex u : g) {
	    if (!u.seen) {
		DFSVisit(u, ++cno);
	    }
	}
	return cno;
    }

    /**
     * Inner method of DFS modified to update the component number for every
     * vertex
     * 
     * @param u
     *            : The vertex that is being visited in DFS order
     * @param cno
     *            : The component number of the vertex
     */
    private static void DFSVisit(Vertex u, int cno) {
	u.seen = true;
	u.cno = cno;
	for (Edge e : u.Adj) {
	    Vertex v = e.otherEnd(u);
	    if (!v.seen) {
		v.parent = u;
		DFSVisit(v, cno);
	    }
	}
    }

    public static void main(String[] args) throws FileNotFoundException {
	Scanner in = new Scanner(new File(args[0]));
	Graph g = Graph.readGraph(in, false); // Read input data from file
	testEulerian(g);
    }
}
