# README #

Description
-----------
Common repository for maintaining Assignment Projects in Implementation of Advanced Data Structures & Algorithms class. All the assignment projects in the class are group projects to be contributed by all members. So we will use this repository to store & manage the code among ourselves.

Contributors
------------
* Giridara Varma
* Praveen Murugesan
* Srikanth Kannan

Usage Guidelines
----------------
Every project will be a separate directory in the repository and we can use Sparse Checkouts to checkout only specific folders. This way we can re-use code between projects without much hustle.

<TODO> Update the commands for sparse-checkout, fork & merge 

Notes
-----
* Always update your current branch & then fork