Long Project 2

Level 2
-------
The project is to implement Edmond's Branching Algorithm to find the Minimum 
Spanning Out-Tree in a Directed Graph.

Contents
--------
* Main - Driver program for LP2 level 2
* Graph, Vertex, Edge - Traditional classes to store details about a Graph 
* SuperVertex, SuperEdge - Super classes of Vertex & Edge respectively to 
	contain extra information about the child vertices and edge replaced by them
* DMST - Class that contains the Directed Minimum Spanning Tree program

Sample Input
------------
The first line should contain the number of vertices & edges. The subsequent 
lines should contain the edge details. By default, vertex '1' is considered the 
root and should have an Out-tree from the root. 
------------
6 10
1 2 5
1 4 8
1 5 10
3 2 7
3 4 3
4 3 2
4 6 6
5 3 4
5 6 2
6 5 1

Sample Output
-------------
The first line contains the weight of the Directed MST. If the number of 
vertices is less than or equal to 50 the individual incoming edges to every node
except the root are printed in the subsequent lines.
-------------
22
(1,2)
(4,3)
(1,4)
(6,5)
(4,6)
