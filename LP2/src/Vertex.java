
/**
 * Class to represent a vertex of a graph
 * 
 * @author G94
 */

import java.util.*;

public class Vertex implements Index, Comparator<Vertex> {
    public int name; // name of the vertex
    public boolean seen; // flag to check if the vertex has already been visited
    public Vertex parent; // parent of the vertex in the MST
    public int distance; // distance to the vertex from the source vertex
    public List<Edge> Adj, revAdj; // adjacency list; use LinkedList or
				   // ArrayList
    public int inDegree; // number of incoming edges in a directed graph
    public int cno; // the component number of the vertex in the graph
    public int index; // the index field used for indexedHeap

    public Edge zeroIn; // 0-weight incoming edge
    public SuperVertex superVertex; // super vertex to replace the current
				    // vertex in a 0-weight cycle
    public int toCycleIndex; // index of the minimum edge to a cycle
    public int fromCycleIndex; // index of the minimum edge from a cycle

    /**
     * Constructor for the vertex
     * 
     * @param n
     *            : int - name of the vertex
     */
    Vertex(int n) {
	name = n;
	seen = false;
	parent = null;
	Adj = new ArrayList<Edge>();
	revAdj = new ArrayList<Edge>(); /* only for directed graphs */
	toCycleIndex = -1;
	fromCycleIndex = -1;
    }

    /**
     * Method to represent a vertex by its name
     */
    public String toString() {
	return Integer.toString(name);
    }

    /*
     * sets the index value of the vertex
     */
    public void putIndex(int i) {
	index = i;
    }

    /*
     * returns the index value of the vertex
     */
    public int getIndex() {
	return index;
    }

    /*
     * compares the distance of two vertices and returns the difference.
     */
    public int compare(Vertex v1, Vertex v2) {
	return v1.distance - v2.distance;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + name;
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Vertex other = (Vertex) obj;
	if (name != other.name)
	    return false;
	return true;
    }
}