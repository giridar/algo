import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Class to implement Edmond's Branching algorithm to find the MST in directed
 * graphs
 * 
 * @author G94
 */
public class DMST {
    /* Find the 0-weight incoming edge in a single vertex */
    public static int reduceWeight(Vertex v) {
	int minWeight = Integer.MAX_VALUE;
	Edge minEdge = null;
	for (Edge e : v.revAdj) {
	    int weight = e.Weight;
	    if (e.From.superVertex == null && weight < minWeight) {
		minWeight = weight;
		minEdge = e;
	    }
	}
	v.zeroIn = minEdge;
	for (Edge e : v.revAdj)
	    if (e.From.superVertex == null)
		e.Weight = e.Weight - minWeight;
	return minWeight;
    }

    /**
     * Find the number of vertices reachable from the source via 0-weight edges
     */
    public static boolean zeroEdgeBFS(Graph g, Vertex root) {
	Queue<Vertex> queue = new LinkedList<>();
	queue.add(root);
	/*
	 * queue is polled until it is empty and every vertex is marked
	 * reachable while also marking it seen for future references
	 */
	while (!queue.isEmpty()) {
	    Vertex u = queue.poll();
	    u.seen = true;
	    g.unseenVertices--;
	    for (Edge e : u.Adj) {
		Vertex v = e.To;
		if (!v.seen && v.superVertex == null && e.Weight == 0)
		    /* Add only 0-weight active paths */
		    queue.add(v);
	    }
	}
	/*
	 * Check if there are still vertices unreachable from root via 0-edges
	 */
	return g.unseenVertices == 0;
    }

    /*
     * Find the parent, of the given vertex, at which a 0-weight cycle is
     * detected
     */
    public static Vertex findCycle(Graph g, Vertex v) {
	Vertex u = null;
	for (u = v; u.cno == 0; u.cno = 1, u = u.zeroIn.From)
	    /*
	     * Walk back from the given vertex using 0-edges until the same
	     * vertex in visited twice
	     */
	    ;
	Vertex parent = u;
	for (u = v; u.name != parent.name; u.cno = 0, u = u.zeroIn.From)
	    /*
	     * Reset the cycle visit count
	     */
	    ;
	return parent;
    }

    /*
     * Find the minimum of all incoming/outgoing edges of the given vertex
     * from/to external vertices
     */
    public static void minEdges(Vertex v, List<Edge> minIn, List<Edge> minOut) {
	for (Edge e : v.revAdj) {
	    Vertex u = e.From;
	    if (u.cno == 1 || u.superVertex != null)
		/* The from vertex is in the cycle or is inactive */
		continue;

	    int fromUIndex = u.toCycleIndex;
	    if (fromUIndex == -1) {
		/* 'u' does not already have a minimum edge into the cycle */
		u.toCycleIndex = minIn.size();
		minIn.add(e);
	    } else if (e.Weight < minIn.get(fromUIndex).Weight)
		/* Current edge is lesser than the previous minimum */
		minIn.set(fromUIndex, e);
	}
	for (Edge e : v.Adj) {
	    Vertex u = e.To;
	    if (u.cno == 1 || u.superVertex != null)
		/* The to vertex is in the cycle or is inactive */
		continue;

	    int toUIndex = u.fromCycleIndex;
	    if (toUIndex == -1) {
		/* 'u' does not already have a minimum edge from the cycle */
		u.fromCycleIndex = minOut.size();
		minOut.add(e);
	    } else if (e.Weight < minOut.get(toUIndex).Weight)
		/* Current edge is lesser than the previous minimum */
		minOut.set(toUIndex, e);
	}
    }

    /*
     * Shrink the 0-weight cycle at the given vertex into a single super vertex
     * and add it to the end of the vertex list.
     */
    public static Vertex shrink(Graph g, Vertex v) {
	List<Edge> minIn = new ArrayList<>();
	List<Edge> minOut = new ArrayList<>();
	List<Vertex> cycleVerts = new ArrayList<>();
	cycleVerts.add(v);
	minEdges(v, minIn, minOut);
	for (Vertex u = v.zeroIn.From; u.name != v.name; u = u.zeroIn.From) {
	    /* Add all the vertices of the cycle in the list and mark them */
	    cycleVerts.add(u);
	    /*
	     * Find the minimum of all incoming/outgoing edges of the cycle
	     * from/to external vertices
	     */
	    minEdges(u, minIn, minOut);
	}

	SuperVertex c = new SuperVertex(g.verts.size(), cycleVerts);
	g.verts.add(c);
	/* 'n' vertices have been shrunken and replaced by 1 super-vertex */
	g.unseenVertices -= cycleVerts.size() - 1;
	for (Vertex u : cycleVerts) {
	    u.superVertex = c;
	    u.cno = 0;
	}
	c.revAdj = new ArrayList<>(minIn.size());
	for (Edge e : minIn) {
	    Vertex from = e.From;
	    SuperEdge in = new SuperEdge(from, c, e.Weight, e);
	    /*
	     * Maintaining the index of the super-edge in the adjacency list
	     * makes it easy while deletion during Expansion.
	     */
	    in.fromIndex = from.Adj.size();
	    in.toIndex = c.revAdj.size();
	    from.Adj.add(in);
	    c.revAdj.add(in);
	    from.toCycleIndex = -1;
	}
	c.Adj = new ArrayList<>(minOut.size());
	for (Edge e : minOut) {
	    Vertex to = e.To;
	    SuperEdge out = new SuperEdge(c, to, e.Weight, e);
	    out.fromIndex = c.Adj.size();
	    out.toIndex = to.revAdj.size();
	    c.Adj.add(out);
	    to.revAdj.add(out);
	    Edge toZeroIn = to.zeroIn;
	    if (toZeroIn != null && toZeroIn.From.name == e.From.name)
		/* Outgoing edge is a 0-weight super-edge */
		to.zeroIn = out;
	    to.fromCycleIndex = -1;
	}

	return c;
    }

    /* Expand the super-vertices */
    public static void expand(Graph g) {
	for (int i = g.verts.size() - 1; i > g.numNodes; i--) {
	    SuperVertex c = (SuperVertex) g.verts.get(i);
	    SuperEdge zeroSuperEdge = (SuperEdge) c.zeroIn;
	    Edge zeroOriginalEdge = zeroSuperEdge.originalEdge;
	    zeroOriginalEdge.To.zeroIn = zeroOriginalEdge;

	    /* Activate the child vertices */
	    for (Vertex u : c.subVertices)
		u.superVertex = null;

	    /* Remove all super-edges */
	    for (Edge e : c.revAdj) {
		SuperEdge se = (SuperEdge) e;
		Vertex from = se.From;
		from.Adj.remove(se.fromIndex);
	    }
	    for (Edge e : c.Adj) {
		SuperEdge se = (SuperEdge) e;
		Vertex to = se.To;
		to.revAdj.remove(se.toIndex);
		Edge toZeroIn = to.zeroIn;
		if (toZeroIn != null && toZeroIn.From.name == c.name)
		    /* Outgoing edge is a 0-weight super-edge */
		    to.zeroIn = se.originalEdge;
	    }

	    g.verts.remove(i);
	}
    }

    /*
     * Find the Minimum Spanning Out-tree of a graph from the given root
     */
    public static long outDMST(Graph g, Vertex root) {
	long wmst = 0;
	for (int v = 1; v < root.name; v++)
	    wmst += reduceWeight(g.verts.get(v));
	for (int v = root.name + 1; v <= g.numNodes; v++)
	    wmst += reduceWeight(g.verts.get(v));

	g.unseenVertices = g.numNodes;
	if (zeroEdgeBFS(g, root))
	    /* All vertices are reachable via 0-weight edges */
	    return wmst;

	/* The graph has 0-weight cycle(s) */
	for (int i = 1; i < g.verts.size(); i++) {
	    Vertex v = g.verts.get(i);
	    if (v.seen || v.superVertex != null)
		/* 'v' is either already in the MST or is inactive */
		continue;

	    /* Find a 0-weight cycle and shrink it to a super-vertex */
	    Vertex c = shrink(g, findCycle(g, v));
	    wmst += reduceWeight(c);
	    if (c.zeroIn.From.seen && zeroEdgeBFS(g, c))
		/*
		 * The super-vertex has a 0-edge from the partial MST and all
		 * vertices are reachable from this super-vertex via 0-edges
		 */
		break;
	}
	/* Expand the super-vertices in the graph */
	expand(g);

	return wmst;
    }
}