import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Class to implement different MST algorithms for Undirected graphs
 * 
 * @author giridar G94
 */
public class MST {
    static final int Infinity = Integer.MAX_VALUE;
    private static int phase = 0;
    private static long startTime, endTime, elapsedTime;

    /**
     * @param g
     *            - input graph for which minimum spanning tree weight is
     *            determined
     * @return - returns the weight of the minimum spanning tree
     */
    static int PrimMST(Graph g) {
	int wmst = 0;
	Vertex src = g.verts.get(1);
	src.seen = true;
	// By default heap is created as twice the number of vertices. As heap
	// size reaches limit, the size gets increased as part of heap operation
	BinaryHeap<Edge> edgeQueue = new BinaryHeap<Edge>(g.numNodes * 2, new Edge());
	for (Edge e : src.Adj)
	    edgeQueue.add(e);

	// Loop Invariant: Heap is initialized with source vertex edges. each
	// edge analyzed and if any one vertex of edge is not yet seen, it is
	// considered as part of MST. Edge gets added to heap accordingly
	// Loop terminates when heap is empty with no more edges to analyze.
	while (!edgeQueue.isEmpty()) {
	    Edge e = edgeQueue.remove();
	    if (e.From.seen && e.To.seen)
		continue;

	    Vertex v = e.From.seen ? e.To : e.From;
	    wmst += e.Weight;
	    v.seen = true;
	    for (Edge edge : v.Adj) {
		if (!edge.otherEnd(v).seen)
		    edgeQueue.add(edge);
	    }
	}

	return wmst;
    }

    /**
     * @param g
     *            - input graph for which minimum spanning tree weight is
     *            determined
     * @return - returns the weight of minimum spanning tree
     */
    static int PrimIndexedMST(Graph g) {
	int wmst = 0;

	// create array of vertices setting all vertices distance as infinity
	// except for the source/first vertices
	Vertex[] verts = new Vertex[g.numNodes];
	int i = 0;
	for (Vertex v : g.verts) {
	    if (v != null) {
		v.distance = Infinity;
		verts[i++] = v;
	    }
	}
	verts[0].distance = 0;

	// set the default index value of each vertices as array index
	for (int j = 0; j < verts.length; j++)
	    verts[j].index = j + 1;

	IndexedHeap<Vertex> vertexQueue = new IndexedHeap<Vertex>(verts, new Vertex(0));

	// Loop Invariant: Indexed heap is initialized with all vertices in
	// graph. Vertex distance is set as minimum of the edge connecting it.
	// Loop terminates when heap is empty
	while (!vertexQueue.isEmpty()) {
	    Vertex u = vertexQueue.remove();
	    u.seen = true;
	    wmst += u.distance;
	    for (Edge e : u.Adj) {
		Vertex v = e.otherEnd(u);
		if (!v.seen && v.distance > e.Weight) {
		    v.distance = e.Weight;
		    v.parent = u;
		    vertexQueue.decreaseKey(v);
		}
	    }
	}

	return wmst;
    }

    public static MSTForest kruskalAlgorithm(Graph g) {
	DisjointSet disjSet = new DisjointSet();
	disjSet.makeSet(g.numEdges + 1);
	MSTForest forest = new MSTForest();

	BinaryHeap<Edge> edgeQueue = new BinaryHeap<Edge>(g.numEdges, new Edge());

	for (Edge e : g.edges)
	    edgeQueue.add(e);

	while (!edgeQueue.isEmpty()) {
	    Edge e = edgeQueue.remove();
	    int ru = disjSet.find(e.From.name);
	    int rv = disjSet.find(e.To.name);
	    if (ru != rv) {
		forest.addToMST(e);
		forest.minimumweight += e.Weight;
		disjSet.union(ru, rv);
	    }
	}

	return forest;
    }

    /**
     * Driver method to test PrimMST, PrimIndexMST and heap sort functionality
     * of Binary heap
     * 
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
	Scanner in;
	if (args.length > 0)
	    in = new Scanner(new File(args[0]));
	else
	    in = new Scanner(System.in);

	// Comparison Prim MST, Prim Index MST and Kruskal MST
	Graph g = Graph.readGraph(in, false);
	System.out.println("Analysis of Prims Algorithm using Binary Heap");
	System.out.println("---------------------------------------------");
	timer();
	System.out.println("Weight of Minimum Spanning Tree is " + PrimMST(g));
	timer();
	g.resetVetices();

	System.out.println("Analysis of Prims Algorithm using Indexed Binary Heap");
	System.out.println("-----------------------------------------------------");
	timer();
	System.out.println("Weight of Minimum Spanning Tree is " + PrimIndexedMST(g));
	timer();
	g.resetVetices();

	System.out.println("Analysis of Kruskals Algorithm using Binary Heap and DisjointSet");
	System.out.println("----------------------------------------------------------------");
	timer();
	MSTForest forest = kruskalAlgorithm(g);
	System.out.println("Weight of Minimum Spanning Tree is " + forest.minimumweight);
	timer();
    }

    /**
     * Timer to calculate the running time
     */
    public static void timer() {
	if (phase == 0) {
	    startTime = System.currentTimeMillis();
	    phase = 1;
	} else {
	    endTime = System.currentTimeMillis();
	    elapsedTime = endTime - startTime;
	    System.out.println("Time: " + elapsedTime + " msec.");
	    memory();
	    phase = 0;
	}
    }

    /**
     * This method determines the memory usage
     */
    public static void memory() {
	long memAvailable = Runtime.getRuntime().totalMemory();
	long memUsed = memAvailable - Runtime.getRuntime().freeMemory();
	System.out.println("Memory: " + memUsed / 1000000 + " MB / " + memAvailable / 1000000 + " MB.");
    }
}
