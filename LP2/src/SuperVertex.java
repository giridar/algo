import java.util.List;

/**
 * Super Vertex to replace a 0-weight cycle
 * 
 * @author G94
 */
public class SuperVertex extends Vertex {
    public List<Vertex> subVertices; // sub-vertices that are represented by
				     // this vertex in a 0-weight cycle

    SuperVertex(int n, List<Vertex> subVertices) {
	super(n);
	this.subVertices = subVertices;
    }
}