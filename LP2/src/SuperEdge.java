/**
 * Super-edge that represents an arc coming into or out of a super vertex.
 * 
 * @author G94
 */
public class SuperEdge extends Edge {
    public Edge originalEdge; // edge in the original graph that this edge
			      // represents
    public int fromIndex; // index of edge in the adjacency list of From
    public int toIndex; // index of edge in the reverse adjacency list of To

    public SuperEdge(Vertex u, Vertex v, int w, Edge e) {
	super(u, v, w);
	originalEdge = e;
    }
}