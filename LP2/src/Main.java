import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Driver class to run Edmond's branching algorithm to find Directed Minimum
 * Spanning Out-tree
 * 
 * @author G94
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
	Scanner in;
	if (args.length > 0)
	    in = new Scanner(new File(args[0]));
	else
	    in = new Scanner(new File("data/lp2-ck.txt"));

	Graph g = Graph.readGraph(in, true);
	Vertex root = g.verts.get(1);

	for (Edge e : g.edges)
	    /* Back-up all weights */
	    e.originalWeight = e.Weight;
	timer();
	long wmst = DMST.outDMST(g, root);
	timer();
	for (Edge e : g.edges)
	    /* Reset all weights */
	    e.Weight = e.originalWeight;

	System.out.println(wmst);
	if (g.numNodes <= 50)
	    for (int i = 2; i <= g.numNodes; i++)
		System.out.println(g.verts.get(i).zeroIn);
    }

    private static int phase = 0;
    private static long startTime, endTime, elapsedTime;

    /**
     * Timer to calculate the running time
     */
    public static void timer() {
	if (phase == 0) {
	    startTime = System.currentTimeMillis();
	    phase = 1;
	} else {
	    endTime = System.currentTimeMillis();
	    elapsedTime = endTime - startTime;
	    System.out.println("Time: " + elapsedTime + " msec.");
	    memory();
	    phase = 0;
	}
    }

    /**
     * This method determines the memory usage
     */
    public static void memory() {
	long memAvailable = Runtime.getRuntime().totalMemory();
	long memUsed = memAvailable - Runtime.getRuntime().freeMemory();
	System.out.println("Memory: " + memUsed / 1000000 + " MB / " + memAvailable / 1000000 + " MB.");
    }
}