import java.util.ArrayList;
import java.util.List;

public class MSTForest {
	private List<Edge> mst;
	public int minimumweight;

	public MSTForest() {
		mst = new ArrayList<>();
		minimumweight = 0;
	}

	public void addToMST(Edge e) {
		mst.add(e);
	}

	public List<Edge> getMST() {
		return mst;
	}

}
