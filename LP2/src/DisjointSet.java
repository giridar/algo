import java.util.Arrays;

public class DisjointSet {
	int[] parent, rank;

	public void makeSet(int x) {
		parent = new int[x];
		rank = new int[x];

		for (int i = 0; i < x; i++)
			parent[i] = i;

		Arrays.fill(rank, 0);
	}

	public int find(int x) {
		if (parent[x] != x)
			parent[x] = find(parent[x]);

		return parent[x];
	}

	public void union(int x, int y) {
		if (rank[x] > rank[y]) {
			parent[y] = x;
		} else if (rank[y] > rank[x]) {
			parent[x] = y;
		} else {
			parent[x] = y;
			rank[y]++;
		}
	}
}
