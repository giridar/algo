public class Entry<T> {
    T element;
    Entry<T> next;
    Entry<T> previous;

    Entry() {
    }

    Entry(T x, Entry<T> nxt, Entry<T> prev) {
	element = x;
	next = nxt;
	previous = prev;
    }
}
