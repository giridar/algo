import java.util.Comparator;

/**
 * @author G94 This class implements comparator where compare method returns
 *         positive when second parameter is less than first parameter
 */
public class IntegerMinComparator implements Comparator<Integer> {
    public int compare(Integer x, Integer y) {
	return x.intValue() - y.intValue();
    }
}
