import java.util.*;

/**
 * @author G94 This class implements comparator where compare method returns
 *         positive when second parameter is greater than first parameter
 */
public class IntegerMaxComparator implements Comparator<Integer> {
    public int compare(Integer x, Integer y) {
	return y.intValue() - x.intValue();
    }
}