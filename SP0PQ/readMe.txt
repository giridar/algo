The main method in MST.java acts as driver method to test SP0PQ project. There are three functionalities that are tested as part of this project
1.	PrimMST - which implements prims algorithm using binary heap
2.	PrimIndexedMST - which implements prims algorithm using indexed heap.
3.	heap sort of Binary Heap - sort an array of elements based on the input comparator instance passed

I PrimMST
Sample Input: Below is an sample input which can be stored in a txt file and passed as an arugument
6 10
1 2 3
1 3 6
1 6 1
2 6 5
2 4 3
3 6 5
3 5 2
4 5 6
4 6 6
5 6 4

Output: Result will be shown as Minimum Spanning Weight:  13. It will take 1 msec to run this small graph in PrimMST

I PrimIndexedMST
Sample Input: Below is an sample input which can be stored in a txt file and passed as an arugument
6 10
1 2 3
1 3 6
1 6 1
2 6 5
2 4 3
3 6 5
3 5 2
4 5 6
4 6 6
5 6 4

Output: Result will be shown as Minimum Spanning Weight:  13. PrimIndexedMST takes the same time as PrimMST, as the graph is very small in size. As the vertices and edges of graph increases, you can see PrimIndexedMST is much faster than PrimMST

III HeapSort of Binary Heap
Sample Input: 
Integer[] a = new Integer[] { 56, 12, 78, 19, 9, 51, 5, 91, 76 };
Also an instance of IntegerMaxComparator is passed to sort the array in increasing order.
Sample Output:
5 5 9 12 19 51 76 78 91

Sample Input: 
Integer[] a = new Integer[] { 56, 12, 78, 19, 9, 51, 5, 91, 76 };
Also an instance of IntegerMinComparator is passed to sort the array in decreasing order.
Sample Output:
91 91 78 76 51 19 12 9 5 



